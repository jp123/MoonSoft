let ADMINISTRADOR = "3";
let ESPECIALISTA = "2";
let EDITOR = "1";
let NINHO = "0"
let ACTIVO = 1;
let INACTIVO = 0;
let usuario;

let app = angular.module("moonSoft", ["ngRoute"]);
app.config(function($routeProvider) {
    $routeProvider
    .when("/", {
        templateUrl : "/publica.html",
        controller : "publicCtrl"
    }) 
    .when("/login",{
        templateUrl :"/login.html",
        controller :"loginCtrl"
    })
    .when("/bienvenido",{
        templateUrl :"/bienvenido.html",
        controller :"welcomeCtrl"
    })
    .when("/registro",{
        templateUrl :"/registro.html",
        controller :"registroCtrl"
    })
    .when("/verBoletines",{
        templateUrl :"/verBoletines.html",
        controller :"verBoletinesCtrl"
    })
    .when("/mision",{
        templateUrl : "/mision.html",
        controller : "misionCtrl"
    })
    .when("/vision",{
        templateUrl : "/vision.html",
        controller : "visionCtrl"
    })
    .when("/loginNinho",{
        templateUrl :"/loginninho.html",
        controller :"loginNinhoCtrl"
    })
    .when("/bienvenidoAdmin",{
        templateUrl :"/perfilAdministrador.html",
        controller :"perfilAdminCtrl"
    })
    .when("/usuarioRol",{
        templateUrl :"/vistaUsuarios.html",
        controller :"usuarioRolCtrl"
    })
    .when("/verPerfil/:id",{
        templateUrl :"/verperfil.html",
        controller :"perfilCtrl"
    })
    .when("/bandejaEntrada",{
        templateUrl :"/bandejaDeEntrada.html",
        controller :"bandejaEntradaCtrl"
    })
    .when("/bandejaSalida",{
        templateUrl :"/bandejaSalida.html",
        controller :"bandejaSalidaCtrl"
    })
    .when("/redactar",{
        templateUrl :"/redactarCarta.html",
        controller :"redactarCartaCtrl"
    })
    .when("/redactarBoletin",{
        templateUrl :"/redactarBoletin.html",
        controller :"redactarBoletinCtrl"
    })
    .when("/verCarta/:id/:tipo",{
        templateUrl :"/verCarta.html",
        controller :"verCartaCtrl"
    })
    .when("/registroNinho",{
        templateUrl :"/inicioNinho.html",
        controller :"registroNinhoCtrl"
    })
    .when("/bienvenidoNinho",{
        templateUrl :"/bienvenidoNinho.html",
        controller :"ninhoCtrl"
    })
    .when("/verAreas",{
        templateUrl :"/verAreas.html",
        controller :"verAreaCtrl"
    })
    .when("/verArea/:id",{
        templateUrl :"/area.html",
        controller :"areaCtrl"
    })
    .when("/preBoletin/:cartaId/:editorId/:espId",{
        templateUrl :"/preBoletin.html",
        controller :"preBoletinCtrl"
    })
    .when("/verPreBoletin/:id",{
        templateUrl :"/verPreBoletin.html",
        controller :"verPreBoletinCtrl"
    })
    .when("/nuevarea",{
        templateUrl :"/crearArea.html",
        controller :"crearAreaCtrl"
    })
    .when("/bandejaAdmin",{
        templateUrl :"/CartasPerdidas.html",
        controller :"perdidasCtrl"
    })
});

app.controller("publicCtrl", publicCtrl);
app.controller("loginCtrl", loginCtrl);
app.controller("welcomeCtrl", welcomeCtrl);
app.controller("registroCtrl", registroCtrl);
app.controller("verBoletinesCtrl", verBoletinesCtrl);
app.controller("loginNinhoCtrl", loginNinhoCtrl);
app.controller("perfilAdminCtrl", perfilAdminCtrl);
app.controller("usuarioRolCtrl", usuarioRolCtrl);
app.controller("perfilCtrl", perfilCtrl);
app.controller("misionCtrl", misionCtrl);
app.controller("visionCtrl", visionCtrl);
app.controller("bandejaEntradaCtrl", bandejaEntradaCtrl);
app.controller("bandejaSalidaCtrl", bandejaSalidaCtrl);
app.controller("redactarCartaCtrl", redactarCartaCtrl);
app.controller("redactarBoletinCtrl", redactarBoletinCtrl);
app.controller("verCartaCtrl", verCartaCtrl);
app.controller("registroNinhoCtrl", registroNinhoCtrl);
app.controller("ninhoCtrl", ninhoCtrl);
app.controller("verAreaCtrl", verAreaCtrl);
app.controller("areaCtrl", areaCtrl);
app.controller("preBoletinCtrl", preBoletinCtrl);
app.controller("verPreBoletinCtrl", verPreBoletinCtrl);
app.controller("crearAreaCtrl", crearAreaCtrl);perdidasCtrl
app.controller("perdidasCtrl", perdidasCtrl);

function publicCtrl($scope, $location, $http){
    let usuarioCtrl = new UsuarioCtrl($http, $location, $scope);
    usuarioCtrl.comprobarSesion();
    $scope.usuario = usuario;
    $scope.toLogin = () => {
        $location.path('/login');
    }
    $scope.inicio = (usuario) => {
        if(usuario.tipo == 1)
            inicioSegunRol(usuario, $location)
        else
            $location.path('/bienvenidoNinho');
    };
}

function loginCtrl($scope, $location, $http){
    let usuarioCtrl = new UsuarioCtrl($http, $location, $scope);
    usuarioCtrl.comprobarSesion();
    $scope.atras = irPrincipal($location)
    $scope.entrar = () => {
        let correo = document.querySelector("#correo").value;
        let contrasenha = document.querySelector("#contrasenha").value;
        usuarioCtrl.ingresoSistema(correo, contrasenha)
    };
}

function loginNinhoCtrl($scope,$location,$http){
    let usuarioCtrl = new UsuarioCtrl($http, $location, $scope);
    let ninhoCtrl = new NinhoCtrl($http, $location, $scope);
    usuarioCtrl.comprobarSesion();
    $scope.atras = irPrincipal($location)
    $scope.entrar = () => {
        let nombre = document.querySelector("#nombre").value
        ninhoCtrl.ingresoSistema(nombre);
    };
}

function misionCtrl($scope, $location, $http){
    let cambiarPagina = true;
    inicializarCtrl($location, $scope, cambiarPagina)
    
}

function visionCtrl($scope, $location, $http){
    let cambiarPagina = true;
    inicializarCtrl($location, $scope, cambiarPagina)
    
}

function registroCtrl($scope, $location, $http){
    let usuarioCtrl = new UsuarioCtrl($http, $location, $scope);
    usuarioCtrl.comprobarSesion();
    $scope.atras = irPrincipal($location)
    let profesionCtrl = new ProfesionCtrl($http, $location, $scope);
    profesionCtrl.obtenerTodos();
    $scope.registrar = () => {
        if(contrasCoinciden()){
           let nuevo_usuario = crearObjetoUsuario();
           usuarioCtrl.registrar(nuevo_usuario);   
        }else{
            alert("Las contraseñas no coinciden");
        }
        
    } 

}

function verBoletinesCtrl($scope, $location, $http){
    let usuarioCtrl = new UsuarioCtrl($http, $location, $scope);
    usuarioCtrl.comprobarSesion();
    $scope.user = usuario;
    let boletinCtrl = new BoletinrCtrl($http, $location, $scope);
    boletinCtrl.obtenerPublicados();
    $scope.descargar = (boletin) => {
        window.location.href = "/api/Archivo/" + boletin.archivo.id;
    }
    $scope.toLogin = () => {
        $location.path('/login');
    }
    $scope.atras = () => {window.history.back();};
}

function registroNinhoCtrl($scope, $location, $http){
    let avatarCtrl = new AvatarCtrl($http, $location, $scope);
    let ninhoCtrl = new NinhoCtrl($http, $location, $scope);
    $scope.ninho = {
        nombre_usuario: "",
        avatar_id: 0
    };
    avatarCtrl.obtenerTodos();
    $scope.seleccionar = (avatar) => {
        ninhoCtrl.obtenerIdMaximo(avatar.nombre_avatar);
        $scope.ninho.avatar_id = avatar.id;
    }
    $scope.guardar = () => {
        ninhoCtrl.guardar($scope.ninho);
    }
}

function welcomeCtrl($scope, $location, $http){
    let usuarioCtrl = new UsuarioCtrl($http, $location, $scope);
    let cambiarPagina = true;
    let privilegios = EDITOR+ESPECIALISTA;
    inicializarCtrl(usuarioCtrl, $location, $scope, cambiarPagina, privilegios)
    $scope.editar = (event) => {
        event.currentTarget.readOnly=false;
    }

    $scope.guardar = (event) => {
        let inputElement = event.currentTarget;
        inputElement.readOnly=true;
        let inputName = inputElement.name
        let value = inputElement.value;
        guardarCambios(usuarioCtrl, usuario, value, inputName);

    }
}

function perfilAdminCtrl($scope, $location, $http){
    let usuarioCtrl = new UsuarioCtrl($http, $location, $scope);
    let cambiarPagina = true;
    let privilegios = ADMINISTRADOR;
    inicializarCtrl(usuarioCtrl, $location, $scope, cambiarPagina, privilegios)

    $scope.editar = (event) => {
        event.currentTarget.readOnly=false;
    }

    $scope.guardar = (event) => {
        let inputElement = event.currentTarget;
        inputElement.readOnly=true;
        let inputName = inputElement.name
        let value = inputElement.value;
        guardarCambios(usuarioCtrl, usuario, value, inputName);

    }
}

function openNav() {
    document.getElementById("sideNavigation").style.width = "250px";
    document.getElementById("main").style.marginLeft = "250px";
}
 
function closeNav() {
    document.getElementById("sideNavigation").style.width = "0";
    document.getElementById("main").style.marginLeft = "0";
}

    // Activate Carousel
    $("#myCarousel").carousel();
      
    // Enable Carousel Indicators
    $(".item1").click(function(){
      $("#myCarousel").carousel(0);
    });
    $(".item2").click(function(){
      $("#myCarousel").carousel(1);
    });
    $(".item3").click(function(){
      $("#myCarousel").carousel(2);
    });
      
    // Enable Carousel Controls
    $(".carousel-control-prev").click(function(){
      $("#myCarousel").carousel("prev");
    });
    $(".carousel-control-next").click(function(){
      $("#myCarousel").carousel("next");
    });

function ninhoCtrl($scope, $location, $http){
    let usuarioCtrl = new UsuarioCtrl($http, $location, $scope);
    let cambiarPagina = true;
    let privilegios = NINHO;
    inicializarCtrl(usuarioCtrl, $location, $scope, cambiarPagina, privilegios)
    $scope.usuario = usuario;

    let bandejaCtrl = new BandejaSalidaCtrl($http, $location, $scope);
    let bandejaEntradaCtrl = new BandejaEntradaCtrl($http, $location, $scope);
    $scope.$watch('usuario', (oldVal, newVal) => {
        if(oldVal == newVal)
            return 
        bandejaCtrl.cartasEnviadas(usuario);  
        bandejaEntradaCtrl.cartasRecibidas(usuario);
    })

    if(usuario){
        bandejaCtrl.cartasEnviadas(usuario);  
        bandejaEntradaCtrl.cartasRecibidas(usuario);
    }

    $scope.eliminarSeleccionados = () => {
        let eliminar = $scope.cartas.filter(carta => carta.seleccionado);
        let elimEn = $scope.cartas_recibidas.filter(carta => carta.seleccionado);
        if(eliminar.length > 0 || elimEn.length > 0){
            let respuesta = confirm("¿Realmente desea eliminar la seleccion?");
            if(respuesta){
                for(let carta of eliminar){
                    bandejaCtrl.eliminarRegistro(carta, usuario);
                }
                for(let carta of elimEn){
                    bandejaEntradaCtrl.eliminarRegistro(carta, usuario);
                }
            }
        }else{
            alert("Nada seleccionado");
        }
    }

    $scope.redactar = () =>{
        $location.path('/redactar');
    }

    $scope.verCarta = (id) => {
        $location.path("/verCarta/"+ id + "/" + 0);
    }
}

function perfilCtrl($scope, $location, $http, $routeParams){
    let usuarioCtrl = new UsuarioCtrl($http, $location, $scope);
    let cambiarPagina = true;
    let privilegios = ADMINISTRADOR+EDITOR+ESPECIALISTA;
    inicializarCtrl(usuarioCtrl, $location, $scope, cambiarPagina, privilegios)

    let usuarioId = $routeParams.id;
    usuarioCtrl.obtenerPorId(usuarioId);
    $scope.atras = () => {window.history.back();};

}

function usuarioRolCtrl($scope, $location, $http){
    let usuarioCtrl = new UsuarioCtrl($http, $location, $scope);
    let cambiarPagina = true;
    let privilegios = ADMINISTRADOR;
    inicializarCtrl(usuarioCtrl, $location, $scope, cambiarPagina, privilegios)

    rolCtrl = new RolCtrl($http, $location, $scope);
    rolCtrl.obtenerTodos();
    usuarioCtrl.obtenerTodos();
    $scope.actualizar = usuarioCtrl.actualizar;
    $scope.desactivarUsuario = (usuario) => {
        usuario.activo = INACTIVO;
        usuarioCtrl.actualizar(usuario);
    }
    $scope.activarUsuario = (usuario) => {
        usuario.activo = ACTIVO;
        usuarioCtrl.actualizar(usuario);
    }
}

function bandejaEntradaCtrl($scope, $location, $http){
    let usuarioCtrl = new UsuarioCtrl($http, $location, $scope);
    let cambiarPagina = true;
    let privilegios = ADMINISTRADOR+EDITOR+ESPECIALISTA;
    inicializarCtrl(usuarioCtrl, $location, $scope, cambiarPagina, privilegios)

    let bandejaCtrl = new BandejaEntradaCtrl($http, $location, $scope);
    bandejaCtrl.cartasRecibidas(usuario);

    $scope.eliminarSeleccionados = () => {
        let eliminar = $scope.cartas.filter(carta => carta.seleccionado);
        if(eliminar.length > 0){
            let respuesta = confirm("¿Realmente desea eliminar la seleccion?");
            if(respuesta){
                for(let carta of eliminar){
                    bandejaCtrl.eliminarRegistro(carta, usuario);
                }
            }
        }else{
            alert("Nada seleccionado");
        }
    }
    $scope.actualizar = () => {
        bandejaCtrl.cartasRecibidas(usuario);
    }
    $scope.verCarta = (id) => {
        bandejaCtrl.marcarVisto(usuario.id, id);
        $location.path("/verCarta/"+ id + "/" + 1);
    }
}

function perdidasCtrl($scope, $location, $http){
    let usuarioCtrl = new UsuarioCtrl($http, $location, $scope);
    let cambiarPagina = true;
    let privilegios = ADMINISTRADOR;
    inicializarCtrl(usuarioCtrl, $location, $scope, cambiarPagina, privilegios)

    let bandejaCtrl = new BandejaEntradaCtrl($http, $location, $scope);
    bandejaCtrl.cartasPerdidas(usuario);

    $scope.actualizar = () => {
        bandejaCtrl.cartasRecibidas(usuario);
    }
    $scope.verCarta = (id) => {
        bandejaCtrl.marcarVisto(usuario.id, id);
        $location.path("/verCarta/"+ id + "/" + 1);
    }
}

function bandejaSalidaCtrl($scope, $location, $http){
    let usuarioCtrl = new UsuarioCtrl($http, $location, $scope);
    let cambiarPagina = true;
    let privilegios = ADMINISTRADOR+EDITOR+ESPECIALISTA;
    inicializarCtrl(usuarioCtrl, $location, $scope, cambiarPagina, privilegios)

    let bandejaCtrl = new BandejaSalidaCtrl($http, $location, $scope);
    bandejaCtrl.cartasEnviadas(usuario);

    $scope.eliminarSeleccionados = () => {
        let eliminar = $scope.cartas.filter(carta => carta.seleccionado);
        if(eliminar.length > 0){
            let respuesta = confirm("¿Realmente desea eliminar la seleccion?");
            if(respuesta){
                for(let carta of eliminar){
                    bandejaCtrl.eliminarRegistro(carta, usuario);
                }
            }
        }else{
            alert("Nada seleccionado");
        }
    }

    $scope.redactar = () =>{
        $location.path('/redactar');
    }

    $scope.verCarta = (id) => {
        $location.path("/verCarta/"+ id + "/" + 0);
    }
}

function redactarCartaCtrl($scope, $location, $http){
    let usuarioCtrl = new UsuarioCtrl($http, $location, $scope);
    let cambiarPagina = true;
    let privilegios = ADMINISTRADOR+EDITOR+ESPECIALISTA+NINHO;
    inicializarCtrl(usuarioCtrl, $location, $scope, cambiarPagina, privilegios);

    let formData = new FormData();
    let cartaCtrl = new CartaCtrl($http, $location, $scope);

    $scope.habilitar = false;

    $scope.carta = {
        contenido : "",
        titulo : " ",
        emisor_id : 0,
        receptor_id: 0,
        tipo_receptor: 0,
        archivo_id: 0,
        carta_respondida_id : 0,
        solicitud : 0
    }

    $scope.revisar = () => {
        let cuestion = $scope.usuarios.find(el => el.id == $scope.carta.receptor_id);
        if (cuestion.rol_id == EDITOR && usuario.rol_id == ESPECIALISTA){
            $scope.habilitar = true;
        }else{
            $scope.habilitar = false;
            $scope.carta.solicitud = 0;
        }
    }

    usuarioCtrl.obtenerTodos();
    $scope.file = () => {
        let fileInput = document.querySelector("#archivo");
        if($scope.carta.solicitud == 1){
            fileInput.accept = ".doc, .docx";
            fileInput.required = true;
        }else{
            fileInput.accept = "";
            fileInput.required = false;
        }
    }
    if($scope.usuarios)
        $scope.usuarios = $scope.usuarios.filter(user => user.id !== usuario.id);
    $scope.enviar = () => {
        $scope.carta.emisor_id = usuario.id;
        if(usuario.tipo !== 0){
            $scope.carta.titulo = document.querySelector("#asunto").value;
            $scope.carta.receptor_id = document.querySelector("#ListaDeProf").value;
        }
        cartaCtrl.guardar($scope.carta, formData);
    }
    $scope.atras = () => {window.history.back();};
    $scope.setTheFiles = function ($files) {
        angular.forEach($files, function (value, key) {
            formData.append('file', value);
        });
    };
}

function redactarBoletinCtrl($scope, $location, $http){
    let usuarioCtrl = new UsuarioCtrl($http, $location, $scope);
    let cambiarPagina = true;
    let privilegios = ADMINISTRADOR+EDITOR+ESPECIALISTA;
    inicializarCtrl(usuarioCtrl, $location, $scope, cambiarPagina, privilegios);
    let boletinCtrl = new BoletinrCtrl($http, $location, $scope);
    let tipo = usuario.rol_id == EDITOR ? 0 : 1;
    boletinCtrl.obtenerTodosPorInvolucrado(usuario.id, tipo);
    $scope.ir = (id) => {
        $location.path("/verPreBoletin/" + id);
    }
    $scope.atras = () => {window.history.back();};
}

function verPreBoletinCtrl($scope, $location, $http, $routeParams){
    let usuarioCtrl = new UsuarioCtrl($http, $location, $scope);
    let cambiarPagina = true;
    let privilegios = ADMINISTRADOR+EDITOR+ESPECIALISTA;
    inicializarCtrl(usuarioCtrl, $location, $scope, cambiarPagina, privilegios)
    let boletinId = $routeParams.id;
    let boletinCtrl = new BoletinrCtrl($http, $location, $scope);
    let cartaCtrl = new CartaCtrl($http, $location, $scope);
    boletinCtrl.obtenerPorId(boletinId);
    $scope.$watch("boletin", (newVal, oldVal) => {
        if (newVal == oldVal)
            return;
        let carta_id = $scope.boletin.carta_id;
        let editor_id = $scope.boletin.editor_id;
        let revisor_id = $scope.boletin.especialista_id;
        cartaCtrl.recuperar(carta_id);
        usuarioCtrl.recuperarPorId(editor_id, (response)=>{
            $scope.editor = response.data;
        });
        usuarioCtrl.recuperarPorId(revisor_id, (response)=>{
            $scope.especialista = response.data;
        });
    })
    $scope.aprobar = () => {
        $scope.boletin.aprobado = 1;
        $scope.boletin.alerta = 1;
        boletinCtrl.actualizar($scope.boletin,
                                (response) => {
                                    alert("El boletin fue aprobado, sera publicado por el editor cuando sea conveniente.");
                                });
    }
    $scope.publicar = () => {
        $scope.boletin.publicado = 1;
        $scope.boletin.alerta = 1;
        boletinCtrl.actualizar($scope.boletin,
                                (response) => {
                                    alert("Publicado exitosamente");
                                    $location.path("/verBoletines");
                                });
    }
    let comentarioCtrl = new ComentarioCtrl($http, $location, $scope);
    $scope.guardarComentario = () => {
        let contenido = document.querySelector("#comentario");
        let comentario = {
            boletin_id : $scope.boletin.id,
            contenido : contenido.value,
            autor : usuario.id
        }
        comentarioCtrl.guardar(comentario);
        contenido.value = "";
    }
    $scope.atras = () => {window.history.back();};
}

function preBoletinCtrl($scope, $location, $http, $routeParams){
    let usuarioCtrl = new UsuarioCtrl($http, $location, $scope);
    let cambiarPagina = true;
    let privilegios = ADMINISTRADOR+EDITOR;
    inicializarCtrl(usuarioCtrl, $location, $scope, cambiarPagina, privilegios)
    let carta_id = $routeParams.cartaId;
    let editor_id = $routeParams.editorId;
    let revisor_id = $routeParams.espId;
    let cartaCtrl = new CartaCtrl($http, $location, $scope);
    cartaCtrl.recuperar(carta_id);
    usuarioCtrl.recuperarPorId(editor_id, (response)=>{
        $scope.editor = response.data;
    });
    usuarioCtrl.recuperarPorId(revisor_id, (response)=>{
        $scope.especialista = response.data;
    });
    let archCtrl = new ArchivoCtrl($http, $location, $scope);
    let boletinCtrl = new BoletinrCtrl($http, $location, $scope);
    let formData = new FormData();
    $scope.boletin = {
        carta_id : carta_id,
        editor_id : editor_id,
        especialista_id : revisor_id,
        titulo : ""
    }
    $scope.crear = () => {
        let texto = document.querySelector("#comentario").value;
        let comentario = {
            contenido : texto,
            autor: usuario.id
        }
        archCtrl.subirArchivo(formData,
                (id)=>{
                    $scope.boletin.archivo_id = id;
                    boletinCtrl.guardar($scope.boletin, comentario);
                });
    }
    $scope.setTheFiles = function ($files) {
        angular.forEach($files, function (value, key) {
            formData.append('file', value);
        });
    };
    $scope.atras = () => {window.history.back();};
}

function verCartaCtrl($scope, $location, $http, $routeParams){
    let usuarioCtrl = new UsuarioCtrl($http, $location, $scope);
    let cambiarPagina = true;
    let privilegios = ADMINISTRADOR+EDITOR+ESPECIALISTA+NINHO;
    inicializarCtrl(usuarioCtrl, $location, $scope, cambiarPagina, privilegios);
    let areaCtrl = new AreaCtrl($http, $location, $scope);
    areaCtrl.obtenerTodos();
    let cartaId = $routeParams.id;
    let formData = new FormData();
    let tipoBandeja = $routeParams.tipo;
    if(tipoBandeja == 1){
        $scope.tipo = "recibida";
    }else{
        $scope.tipo = "enviada";
    }
    $scope.respuesta = true;
    $scope.$watch("carta_enviada", (newVal, oldVal)=>{
        if (newVal == oldVal)
            return
        if(usuario.rol_id == ADMINISTRADOR && $scope.carta_enviada.tipo_emisor == 0)
            $scope.tipo = "admin";
        let tipo_receptor = $scope.carta_enviada.tipo_emisor == 0 ? -1:0;
        $scope.carta = {
            contenido : "",
            titulo : " ",
            emisor_id : 0,
            receptor_id: $scope.carta_enviada.emisor_id,
            tipo_receptor: tipo_receptor,
            archivo_id: 0,
            carta_respondida_id : $scope.carta_enviada.id,
            solicitud : 0 
        } 
    });
    usuarioCtrl.obtenerEditores();
    let cartaCtrl = new CartaCtrl($http, $location, $scope);
    cartaCtrl.recuperar(cartaId);
    $scope.habilitar = ()=>{
        let button = document.querySelector("#responder");
        let div = document.querySelector('#div-resp');
        if(button.textContent == 'Responder'){
            button.textContent = "Cancelar";
            button.classList.remove("btn-primary");
            button.classList.add("btn-danger");
            div.classList.remove('hidden');
        }else{
            button.textContent = "Responder";
            button.classList.remove("btn-danger");
            button.classList.add("btn-primary");
            div.classList.add('hidden');
        }
    }

    $scope.enviar = () => {
        $scope.carta.emisor_id = usuario.id;
        $scope.carta.titulo = document.querySelector("#asunto").value;
        cartaCtrl.guardar($scope.carta, formData);
    }
    $scope.atras = () => {window.history.back();};
    $scope.setTheFiles = function ($files) {
        angular.forEach($files, function (value, key) {
            formData.append('file', value);
        });
    };
    $scope.crearBoletin = () => {
        $location.path("/preBoletin/" + $scope.carta_enviada.id + "/" + usuario.id + "/" + $scope.carta_enviada.emisor_id);
    }
    $scope.actualizar= (carta) => {
        cartaCtrl.actualizar(carta);
    }
}

function verAreaCtrl($scope, $location, $http){
    let usuarioCtrl = new UsuarioCtrl($http, $location, $scope);
    let cambiarPagina = true;
    let privilegios = ADMINISTRADOR;
    inicializarCtrl(usuarioCtrl, $location, $scope, cambiarPagina, privilegios);   

    let areaCtrl = new AreaCtrl($http, $location, $scope);
    areaCtrl.obtenerTodos();
}

function areaCtrl($scope, $location, $http, $routeParams){
    let usuarioCtrl = new UsuarioCtrl($http, $location, $scope);
    let cambiarPagina = true;
    let privilegios = ADMINISTRADOR;
    inicializarCtrl(usuarioCtrl, $location, $scope, cambiarPagina, privilegios);   

    let id = $routeParams.id;
    let areaCtrl = new AreaCtrl($http, $location, $scope);
    let profesionCtrl = new ProfesionCtrl($http, $location, $scope);
    areaCtrl.obtenerPorId(id);

    $scope.parametro = () => {
        let input = document.querySelector("#parametro");
        if(input.value !== ""){
            input.value = input.value.toLowerCase();
            $scope.area.parametros.push(input.value);
            $scope.area.parametros.sort();
            input.value = "";
            $scope.area.diccionario = JSON.stringify($scope.area.parametros);
            areaCtrl.actualizar($scope.area);
        }
    }

    $scope.profesion = () => {
        let input = document.querySelector("#profesion");
        if(input.value !== ""){
            $scope.area.profesiones.push({nombre: input.value});
            $scope.area.profesiones.sort();
            profesionCtrl.guardar({
                nombre: input.value,
                areaId: $scope.area.id
            });
            input.value = "";
        }
    }

    $scope.atras = () => {window.history.back();};
}

function crearAreaCtrl($scope, $location, $http){
    let usuarioCtrl = new UsuarioCtrl($http, $location, $scope);
    let cambiarPagina = true;
    let privilegios = ADMINISTRADOR;
    inicializarCtrl(usuarioCtrl, $location, $scope, cambiarPagina, privilegios);   

    let areaCtrl = new AreaCtrl($http, $location, $scope);
    $scope.area = {
        nombre: "",
        diccionario: [],
        profesiones: []
    }
    $scope.parametro = () => {
        let input = document.querySelector("#parametro");
        if(input.value !== ""){
            $scope.area.diccionario.push(input.value);
            $scope.area.diccionario.sort();
            input.value = "";
        }
    }

    $scope.profesion = () => {
        let input = document.querySelector("#profesion");
        if(input.value !== ""){
            $scope.area.profesiones.push(input.value);
            $scope.area.profesiones.sort();
            input.value = "";
        }
    }

    $scope.guardar = () => {
        $scope.area.diccionario = JSON.stringify($scope.area.diccionario);
        areaCtrl.guardar($scope.area);
    }

    $scope.validar = (e) => {
        let txt = e.currentTarget;
        if(e.keyCode == 32){
            //txt.value += " ";
            //txt.value = txt.value.substring(0, txt.value.length - 1);
            txt.value = txt.value.replace(/\s/g, "")
        }
    }

    $scope.atras = () => {window.history.back();};
}

app.directive('ngFiles', ['$parse', function ($parse) {

    function file_links(scope, element, attrs) {
        let onChange = $parse(attrs.ngFiles);
        element.on('change', function (event) {
            onChange(scope, {$files: event.target.files});
        });
    }

    return {
        link: file_links
    }
}]);