function irPrincipal(location){
    return () => {
        location.path("/")
    }
}

function crearObjetoUsuario(){
    nombreCompleto = document.querySelector("#nombre").value;
    direccion = document.querySelector("#direccion").value;
    telefono = document.querySelector("#telefono").value;
    profesion = document.querySelector("#profesion").value;
    email = document.querySelector("#email").value;
    contrasenha = document.querySelector("#contrasenha").value;
    
    return {
            nombre_completo : nombreCompleto,
            direccion : direccion,
            telefono : telefono,
            profesion_id : profesion,
            correo : email,
            contrasenha : contrasenha
    }
}

function comprobarPrivilegios(usuarioCtrl, location, cambiarPagina, privilegios){
    if(usuario == undefined){
        usuarioCtrl.comprobarSesion(cambiarPagina);
    }
    if(usuario!==undefined && !privilegios.includes(usuario.rol_id)){
        location.path("/");
    }
}

function inicioSegunRol(usuario, location){
    if(usuario.rol_id == ADMINISTRADOR ){
        location.path("/bienvenidoAdmin")    
    }else{
        location.path("/bienvenido")
    }
}

function cargarMenuPorRol(location, scope, rol){
    switch(rol){
        case ADMINISTRADOR:
            scope.usuarios = () => {location.path("/usuarioRol")}
            break;
        case ESPECIALISTA:
            break;
        case EDITOR:
            break;
    }

}

function inicializarCtrl(usuarioCtrl, location, scope, cambiarPagina, privilegios){
    comprobarPrivilegios(usuarioCtrl, location, cambiarPagina, privilegios);
    scope.user = usuario;
    scope.$watch("user", (oldValue, newValue)=>{
        if(oldValue == newValue)
            return;
        cargarMenuPorRol(location, scope, usuario.rol);  
    });
    scope.cerrarSesion = usuarioCtrl.cerrarSesion;
}

function guardarCambios(usuarioCtrl, usuario, valor, atributo){
    let huboCambio = false;
    switch(atributo){
        case "Nombre":
            huboCambio = usuario.nombre_completo !== valor;
            usuario.nombre_completo = valor;
        break;
        case "Correo":
            huboCambio = usuario.correo !== valor;
            usuario.correo = valor;
        break;
        case "Direccion":
            huboCambio = usuario.direccion !== valor;
            usuario.direccion = valor;
        break;
        case "Telefono":
            huboCambio = usuario.telefono !== valor;
            usuario.telefono = valor;
        break;
        case "Profesion":
            huboCambio = usuario.profesion !== valor;
            usuario.profesion = valor;
        break;
    }

    if(huboCambio){
        usuarioCtrl.actualizar(usuario)
    }
}
