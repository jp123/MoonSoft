class BandejaEntradaCtrl{
    constructor(http, location, scope){
        this.restService = new RestService(http)
        this.locationService = location;
        this.model = scope;
        this.cartasRecibidas = this.cartasRecibidas.bind(this);
        this.eliminarRegistro = this.eliminarRegistro.bind(this);
        this._agregarSeleccionadoControl = this._agregarSeleccionadoControl.bind(this);
    }

    cartasRecibidas(usuario){
        this.restService.get("/BandejaEntrada/" + usuario.id + "/" + usuario.tipo, 
                            (response)=>{
                                this._agregarSeleccionadoControl(response.data);
                                if(usuario.tipo == 1){
                                    this.model.cartas = response.data;
                                    if(usuario.rol_id == EDITOR || usuario.rol_id == ADMINISTRADOR){
                                        this.model.cartas = this.model.cartas.filter(el => el.tipo_emisor == 1);
                                    }
                                }
                                else{
                                    this.model.cartas_recibidas = response.data;
                                }
                            },
                            (error)=>{
                                console.error(error)
                            });
    }

    cartasPerdidas(usuario){
        this.restService.get("/BandejaEntrada/" + usuario.id + "/" + usuario.tipo, 
                            (response)=>{
                                this._agregarSeleccionadoControl(response.data);
                                this.model.cartas = response.data;
                                this.model.cartas = this.model.cartas.filter(el => el.tipo_emisor == 0 && el.area_id == 4);
                            },
                            (error)=>{
                                console.error(error)
                            });
    }

    _agregarSeleccionadoControl(cartas){
        for(let carta of cartas){
            carta.seleccionado = false;
        }
        cartas=cartas.reverse();
    }

    eliminarRegistro(carta, usuario){
        this.restService.delete("/borrarEntrada/"+ carta.id +"/"+ usuario.id + "/" + usuario.tipo, 
                            (response)=>{
                                this.cartasRecibidas(usuario);
                            },
                            (error)=>{
                                console.error(error)
                            });
    }

    marcarVisto(usuario_id, carta_id){
        this.restService.post("/visto/" + usuario_id + "/" + carta_id,
                             {},
                             (response)=>{

                             },
                             (error)=>{

                             })
    }
}