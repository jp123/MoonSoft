class ComentarioCtrl{
    constructor(http, location, scope){
        this.restService = new RestService(http)
        this.locationService = location;
        this.model = scope;
        this.guardar = this.guardar.bind(this);
    }

    guardar(comentario){
        this.restService.post("/Comentario", 
                            comentario,
                            (response)=>{
                                this.model.boletin.comentarios = response.data.reverse();
                            },
                            (error)=>{
                                console.error(error)
                            });
    }

}