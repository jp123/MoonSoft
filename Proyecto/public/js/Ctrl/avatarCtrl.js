class AvatarCtrl{
    constructor(http, location, scope){
        this.restService = new RestService(http)
        this.locationService = location;
        this.model = scope;
        this.obtenerTodos = this.obtenerTodos.bind(this);
        this.obtenerPorId = this.obtenerPorId.bind(this);
    }

    obtenerTodos(){
        this.restService.get("/Avatars", 
                            (response)=>{
                                this.model.avatars = response.data;
                            },
                            (error)=>{
                                console.error(error)
                            });
    }

    obtenerPorId(id){
        this.restService.get("/Avatar/" + id, 
                            (response)=>{
                                this.model.avatar = response.data;
                            },
                            (error)=>{
                                console.error(error)
                            });
    }
}