class BoletinrCtrl{
    constructor(http, location, scope){
        this.restService = new RestService(http)
        this.locationService = location;
        this.model = scope;
        this.comentarioCtrl = new ComentarioCtrl(http, location, scope);
        this.guardar = this.guardar.bind(this);
        this.obtenerPorId = this.obtenerPorId.bind(this);
        this.obtenerTodosPorInvolucrado = this.obtenerTodosPorInvolucrado.bind(this);
    }

    guardar(boletin, comentario){
        this.restService.post("/Boletin",
                            boletin,
                            (response)=>{
                                let id = response.data;
                                if(comentario.contenido !== ""){
                                    comentario.boletin_id = id;
                                    this.comentarioCtrl.guardar(comentario);
                                }
                                this.locationService.path("/verPreBoletin/" + id);
                            },
                            (error)=>{
                                console.error(error)
                            });
    }

    obtenerPorId(id){
        this.restService.get("/Boletin/" + id, 
                            (response)=>{
                                this.model.boletin = response.data;
                                this.model.boletin.comentarios = this.model.boletin.comentarios.reverse();
                            },
                            (error)=>{
                                console.error(error)
                            });
    }

    obtenerTodosPorInvolucrado(id, tipo){
        this.restService.get("/Boletines/" + id + "/" + tipo, 
                            (response)=>{
                                this.model.boletines = response.data;
                            },
                            (error)=>{
                                console.error(error);
                            });
    }

    actualizar(boletin, callback){
        this.restService.put("/Boletin/" + boletin.id,
                            boletin,
                            callback,
                            (error)=>{
                                console.error(error);
                            });

    }

    obtenerPublicados(){
        this.restService.get("/BoletinesPublicos", 
                            (response)=>{
                                this.model.boletines = response.data.reverse();
                            },
                            (error)=>{
                                console.error(error);
                            });
    }
}