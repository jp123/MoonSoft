class RestService{
    GET = "GET";
    POST = "POST"
    PUT = "PUT"
    DELETE = "DELETE"
    APIUrl = "/api"

    constructor(http){
        this.httpService = http;
        this.get = this.get.bind(this);
        this.post = this.post.bind(this);
        this.put = this.put.bind(this);
        this.delete = this.delete.bind(this);
        this.uploadFile = this.uploadFile.bind(this);
        this.consumeAPI = this.consumeAPI.bind(this);
    }

    get(path, successCallback, errorCallback){
        let req = {
            method: this.GET,
            url: this.APIUrl + path,
        }

        this.consumeAPI(req, successCallback, errorCallback);
    }

    post(path, obj, successCallback, errorCallback){
        let req = {
            method: this.POST,
            url: this.APIUrl + path,
            headers: {
                'Content-Type': 'application/json'
            },
            data : JSON.stringify(obj)
        }

        this.consumeAPI(req, successCallback, errorCallback);
    }

    uploadFile(path, formData, successCallback, errorCallback){
        let req = {
            method: this.POST,
            url: this.APIUrl + path,
            data: formData,
            headers: {
                'Content-Type': undefined
            }
        }

        this.consumeAPI(req, successCallback, errorCallback);
    }

    put(path, obj, successCallback, errorCallback){
        let req = {
            method: this.PUT,
            url: this.APIUrl + path,
            headers: {
                'Content-Type': 'application/json'
            },
            data : JSON.stringify(obj)
        }

        this.consumeAPI(req, successCallback, errorCallback);
    }

    delete(path, successCallback, errorCallback){
        let req = {
            method: this.DELETE,
            url: this.APIUrl + path
        }

        this.consumeAPI(req, successCallback, errorCallback);
    }

    consumeAPI(req, successCallBack, errorCallBack){
        this.httpService(req)
        .then(successCallBack)
        .catch(errorCallBack);
    }

}