class NinhoCtrl{
    key = "usuario_id";
    USUARIO_Ninho = "ninho"
    constructor(http, location, scope){
        this.restService = new RestService(http)
        this.locationService = location;
        this.model = scope;
        this.obtenerTodos = this.obtenerIdMaximo.bind(this);
        this._guardarUsuarioIdEnLocalStorage = this._guardarUsuarioIdEnLocalStorage.bind(this);
        this._obtenerUsuarioIdDeLocalStorage = this._obtenerUsuarioIdDeLocalStorage.bind(this);
        this._quitarUsuarioIdDeLocalStorage = this._quitarUsuarioIdDeLocalStorage.bind(this);
    }

    obtenerIdMaximo(avatar){
        this.restService.get("/maxId", 
                            (response)=>{
                                let input = document.querySelector("#usuarioninho");
                                let userName = avatar + (parseInt(response.data) + 1); 
                                this.model.ninho.nombre_usuario = userName;
                                input.value = userName;
                            },
                            (error)=>{
                                console.error(error)
                            });
    }

    guardar(ninho){
        this.restService.post("/Ninho",
                              ninho,
                              (response) => {
                                  alert("¡Felicidades!. Ya eres un superheroe");
                                  this.ingresoSistema(ninho.nombre_usuario);
                              },
                              (error)=>{
                                  console.error(error);
                              })
    }

    ingresoSistema(nombre){
        this.restService.get("/Ninho/" + nombre,
                             (response)=>{
                                 usuario = response.data;
                                 if(usuario.activo == ACTIVO){
                                    this._guardarUsuarioIdEnLocalStorage(usuario.nombre_usuario);
                                    usuario.tipo = 0;
                                    usuario.rol_id = 0;
                                    this.locationService.path("/bienvenidoNinho");
                                }
                                else{
                                    alert("Su cuenta fue desactivada, comuniquese con el administrador");
                                    this.cerrarSesion();
                                }
                             }
        );
    }
    
    cerrarSesion(){
        usuario = undefined;
        this._quitarUsuarioIdDeLocalStorage();
        this.locationService.path("/");
    }

    comprobarSesion(changePage){
        if(usuario == undefined){
            let id = this._obtenerUsuarioIdDeLocalStorage();
            if(id){
                this.ingresoSistema(id);
            }else if(changePage){
                this.locationService.path("/")
            }
        }
    }

    _guardarUsuarioIdEnLocalStorage(usuario_id){
        localStorage.setItem(this.key, usuario_id);
        localStorage.setItem("tipo", this.USUARIO_Ninho);
    }

    _quitarUsuarioIdDeLocalStorage(){
        localStorage.removeItem(this.key);
        localStorage.removeItem("tipo");
    }

    _obtenerUsuarioIdDeLocalStorage(){
        let tipo = localStorage.getItem("tipo");
        if(tipo){
            if(tipo == this.USUARIO_Ninho){
                return localStorage.getItem(this.key);        
            }else{
                return undefined;
            }
        }
        return undefined;
    }

}