class ProfesionCtrl{
    constructor(http, location, scope){
        this.restService = new RestService(http)
        this.locationService = location;
        this.model = scope;
        this.obtenerTodos = this.obtenerTodos.bind(this);
        this.guardar = this.guardar.bind(this);
    }

    guardar(profesion){
        this.restService.post("/Profesion",
                              profesion,
                              (response)=>{

                              },
                              (error)=>{
                                console.error(error)
                            });
    }

    obtenerTodos(){
        this.restService.get("/profesiones", 
                            (response)=>{
                                this.model.profesiones = response.data;
                                this.model.profesiones = this.model.profesiones.filter(el => el.id != 7);
                            },
                            (error)=>{
                                console.error(error)
                            });
    }
}