class AreaCtrl{
    constructor(http, location, scope){
        this.restService = new RestService(http)
        this.locationService = location;
        this.model = scope;
        this.obtenerTodos = this.obtenerTodos.bind(this);
        this.obtenerPorId = this.obtenerPorId.bind(this);
        this.guardar = this.guardar.bind(this);
        this.actualizar = this.actualizar.bind(this);
    }

    guardar(area){
        this.restService.post("/Area",
                             area,
                            (response) => {
                                alert("Nueva area creada con exito");
                                this.locationService.path("/verArea/" + response.data.id);
                            },
                            (error)=>{
                                console.error(error)
                            });
    }

    actualizar(area){
        this.restService.put("/Area/" + area.id,
                             area,
                            (response) => {
                                
                            },
                            (error)=>{
                                alert("No se pudo guardar");
                                this.locationService.path("/verArea/" + area.id);
                                console.error(error)
                            });
    }

    obtenerTodos(){
        this.restService.get("/Areas", 
                            (response)=>{
                                let areas = response.data;
                                areas = areas.filter(el => el.id !=4);
                                this.model.areas = areas;
                            },
                            (error)=>{
                                console.error(error)
                            });
    }

    obtenerPorId(id){
        this.restService.get("/Area/" + id, 
                            (response)=>{
                                this.model.area = response.data;
                            },
                            (error)=>{
                                console.error(error)
                            });
    }
}