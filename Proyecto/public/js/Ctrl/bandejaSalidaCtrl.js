class BandejaSalidaCtrl{
    constructor(http, location, scope){
        this.restService = new RestService(http)
        this.locationService = location;
        this.model = scope;
        this.cartasEnviadas = this.cartasEnviadas.bind(this);
        this.eliminarRegistro = this.eliminarRegistro.bind(this);
        this._agregarSeleccionadoControl = this._agregarSeleccionadoControl.bind(this);
    }

    cartasEnviadas(usuario){
        this.restService.get("/BandejaSalida/" + usuario.id + "/" + usuario.tipo, 
                            (response)=>{
                                this._agregarSeleccionadoControl(response.data);
                                this.model.cartas = response.data;
                            },
                            (error)=>{
                                console.error(error)
                            });
    }

    _agregarSeleccionadoControl(cartas){
        for(let carta of cartas){
            carta.seleccionado = false;
        }
        cartas = cartas.reverse();
    }

    eliminarRegistro(carta, usuario){
        this.restService.delete("/borrarSalida/"+ carta.id +"/"+ usuario.id + "/" + usuario.tipo, 
                            (response)=>{
                                this.cartasEnviadas(usuario);
                            },
                            (error)=>{
                                console.error(error)
                            });
    }
}