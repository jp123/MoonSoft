class ArchivoCtrl{
    constructor(http, location, scope){
        this.restService = new RestService(http)
        this.locationService = location;
        this.model = scope;
        this.subirArchivo = this.subirArchivo.bind(this);
    }

    subirArchivo(formData, callback){
        this.restService.uploadFile("/Archivo", 
                                    formData,
                                    (response)=>{
                                        let id = response.data;
                                        callback(id);
                                    },
                                    (error)=>{
                                        console.error(error);
                                    }
                                    );
    }

}