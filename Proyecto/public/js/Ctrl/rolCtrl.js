class RolCtrl{
    constructor(http, location, scope){
        this.restService = new RestService(http)
        this.locationService = location;
        this.model = scope;
        this.obtenerTodos = this.obtenerTodos.bind(this);
    }

    obtenerTodos(){
        this.restService.get("/roles", 
                            (response)=>{
                                this.model.roles = response.data;
                            },
                            (error)=>{
                                console.error(error)
                            });
    }
}