class CartaCtrl{
    constructor(http, location, scope){
        this.restService = new RestService(http)
        this.locationService = location;
        this.model = scope;
        this.archCtrl = new ArchivoCtrl(http, location, scope);
        this.guardar = this.guardar.bind(this);
        this.enviar = this.enviar.bind(this);
        this.recuperar = this.recuperar.bind(this);
    }

    guardar(carta, formData){
        if(formData.get('file')){
            this.archCtrl.subirArchivo(formData,
                                        (id)=>{
                                            carta.archivo_id = id;
                                            this.enviar(carta);
                                        });
        }else{
            carta.archivo_id = 0;
            this.enviar(carta);
        }
    }

    actualizar(carta){
        this.restService.put("/Carta/" + carta.id,
                             carta,
                            (response) => {
                                alert("La carta fue enviada a los responsables del area seleccionado");
                                this.locationService.path('/bandejaAdmin');
                            },
                            (error)=>{
                                this.locationService.path('/bandejaAdmin');
                            });
    }

    enviar(carta){
        let path = "";
        if(usuario.tipo == 1)
            path = "/enviar"
        else
            path = "/enviarNinho"
        this.restService.post(path, 
                              carta,
                              (response) => {
                                  alert("Se envio la carta exitosamente.");
                                  if(usuario.tipo == 1)
                                  this.locationService.path("/bandejaSalida");
                                  else
                                  this.locationService.path("/bienvenidoNinho");
                              },
                              (error)=>{
                                  console.error(error);
                              }
                            );
    }

    recuperar(id){
        this.restService.get("/Carta/" + id,
                            (response) => {
                                this.model.carta_enviada = response.data.carta;
                                this.model.conversacion = response.data.conversacion;
                                if(this.model.carta_enviada.solicitud == 1 && this.model.carta_enviada.tipo_emisor == 1 && usuario.rol_id == '1'){
                                    this.model.tipo = "boletin";
                                }
                            },
                            (error) => {
                                console.error(error);
                            }
        ) 
    }

}