class UsuarioCtrl{
    key = "usuario_id";
    USUARIO_NOMRAL = "normal";
    constructor(http, location, scope){
        this.restService = new RestService(http)
        this.locationService = location;
        this.model = scope;
        this.ingresoSistema = this.ingresoSistema.bind(this);
        this.recuperarUsuario = this.recuperarUsuario.bind(this);
        this.comprobarSesion = this.comprobarSesion.bind(this);
        this.cerrarSesion = this.cerrarSesion.bind(this);
        this.obtenerTodos = this.obtenerTodos.bind(this);
        this.actualizar = this.actualizar.bind(this);
        this._guardarUsuarioIdEnLocalStorage = this._guardarUsuarioIdEnLocalStorage.bind(this);
        this._obtenerUsuarioIdDeLocalStorage = this._obtenerUsuarioIdDeLocalStorage.bind(this);
        this._quitarUsuarioIdDeLocalStorage = this._quitarUsuarioIdDeLocalStorage.bind(this);
        this._redirigirBienvenido = this._redirigirBienvenido.bind(this);
    }

    obtenerTodos(){
        this.restService.get("/todosUsuarios", 
                            (response)=>{
                                this.model.usuarios = response.data;
                            },
                            (error)=>{
                                console.error(error)
                            });
    }

    obtenerEditores(){
        this.restService.get("/todosUsuarios", 
                            (response)=>{
                                this.model.usuarios = response.data;
                                this.model.usuarios = this.model.usuarios.filter(el => el.rol_id == "1");
                            },
                            (error)=>{
                                console.error(error)
                            });
    }

    ingresoSistema(correo, contrasenha){
        let credenciales = {
            correo:correo,
            contrasenha:contrasenha
        };

        this.restService.post("/login", 
                            credenciales,
                            this._redirigirBienvenido,
                            (error)=>{
                                console.error(error)
                                alert("El usuario no existe.\nLo invitamos a que se registre.")
                            });
    }

    registrar(usuario){
        this.restService.post("/Usuario", 
                            usuario,
                            (response)=>{
                                if(response.status == 200){
                                    this.ingresoSistema(usuario.correo, usuario.contrasenha);
                                }
                            },
                            (error)=>{
                                console.error(error)
                            });
    }

    actualizar(usuario){
        this.restService.put("/Usuario/" + usuario.id, 
                            usuario,
                            (response)=>{
                                if(response.status == 200){
                                    alert("Actualizacion correcta");
                                }
                            },
                            (error)=>{
                                alert("Hubo un problema :(");
                                console.error(error)
                            });
    }

    comprobarSesion(changePage){
        if(usuario == undefined){
            let id = this._obtenerUsuarioIdDeLocalStorage();
            if(id){
                this.recuperarUsuario(id);
            }else if(changePage){
                let ninhoCtrl = new NinhoCtrl(this.restService.httpService, this.locationService, this.model);
                ninhoCtrl.comprobarSesion();
                this.locationService.path("/")
            }else{
                let ninhoCtrl = new NinhoCtrl(this.restService.httpService, this.locationService, this.model);
                ninhoCtrl.comprobarSesion();
            }
        }
    }

    cerrarSesion(){
        usuario = undefined;
        this._quitarUsuarioIdDeLocalStorage();
        this.locationService.path("/");
    }

    _guardarUsuarioIdEnLocalStorage(usuario_id){
        localStorage.setItem(this.key, usuario_id);
        localStorage.setItem("tipo", this.USUARIO_NOMRAL);
    }

    _quitarUsuarioIdDeLocalStorage(){
        localStorage.removeItem(this.key);
        localStorage.removeItem("tipo");
    }

    _obtenerUsuarioIdDeLocalStorage(){
        let tipo = localStorage.getItem("tipo");
        if(tipo){
            if(tipo == this.USUARIO_NOMRAL){
                return localStorage.getItem(this.key);        
            }else{
                return undefined;
            }
        }
        return undefined;
    }

    _redirigirBienvenido(response){
        let res = response.data;
        if(res.correcto){
            this.recuperarUsuario(res.id_usuario);
        }else{
            alert("Contraseña o usuario incorrecto")
        }
    }

    obtenerPorId(id){
        this.restService.get("/Usuario/" + id,
                            (response)=>{
                                this.model.user = response.data;
                            },
                            (error)=>{
                                console.error(error);
                            })
    }

    recuperarPorId(id, callback){
        this.restService.get("/Usuario/" + id,
                            callback,
                            (error)=>{
                                console.error(error);
                            })
    }

    recuperarUsuario(id){
        this.restService.get("/Usuario/" + id, 
                            (response)=>{
                                usuario = response.data;
                                this.model.user = usuario;
                                if(usuario.activo == ACTIVO){
                                    usuario.tipo = 1;
                                    this._guardarUsuarioIdEnLocalStorage(usuario.id);
                                    inicioSegunRol(usuario, this.locationService);
                                }
                                else{
                                    alert("Su cuenta fue desactivada, comuniquese con el administrador");
                                    this.cerrarSesion();
                                }
                            },
                            (error)=>{
                                console.error(error)
                            });
    }
}