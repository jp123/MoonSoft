<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Comentarios extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('comentarios', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('boletin_id');
            $table->integer('autor');
            $table->string('contenido');
            $table->smallInteger('estado');
            $table->timestamps();
        });
        Schema::table('boletines', function($table) {
            $table->smallInteger('alerta');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('comentarios');
        Schema::table('boletines', function($table) {
            $table->dropColumn('alerta');
        });
    }
}
