<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Solicitud extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('cartas', function($table) {
            $table->smallInteger('solicitud');
            $table->smallInteger('atendido');
        });
        Schema::table('boletines', function($table) {
            $table->string('titulo');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('cartas', function($table) {
            $table->dropColumn('solicitud');
            $table->dropColumn('atendido');
        });
        Schema::table('boletines', function($table) {
            $table->dropColumn('titulo');
        });
    }
}
