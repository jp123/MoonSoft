<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Archivo extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('archivos', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nombre_archivo');
            $table->integer('tamanho');
            $table->string('tipo');
            $table->timestamps();
        });
        Schema::table('cartas', function($table) {
            $table->integer('archivo_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('archivos');
        Schema::table('cartas', function($table) {
            $table->dropColumn('archivo_id');
        });
    }
}
