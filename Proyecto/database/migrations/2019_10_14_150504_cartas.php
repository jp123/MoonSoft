<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Cartas extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ninhos', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nombre_usuario');
            $table->smallInteger('activo');
            $table->timestamps();
        });

        Schema::create('cartas', function (Blueprint $table) {
            $table->increments('id');
            $table->text('contenido');
            $table->string('titulo');
            $table->integer('emisor_id');
            $table->smallInteger('tipo_emisor');
            $table->integer('receptor_id');
            $table->integer('area_id');
            $table->date('fecha_emision');
            $table->timestamps();
        });

        Schema::create('areas', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nombre');
            $table->timestamps();
        });

        Schema::create('profesions', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nombre');
            $table->integer('area_id');
            $table->timestamps();
        });

        Schema::table('usuarios', function($table) {
            $table->dropColumn('profesion');
            $table->integer('profesion_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cartas');
        Schema::dropIfExists('ninhos');
        Schema::dropIfExists('areas');
        Schema::dropIfExists('profesions');
    }
}
