<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Bandejas extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bandeja_entradas', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('duenho_id');
            $table->integer('area_id');
            $table->timestamps();
        });

        Schema::create('bandeja_salidas', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('dueño_id');
            $table->smallInteger('tipo_duenho');
            $table->timestamps();
        });

        Schema::create('cartas_bandeja_entradas', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('bandeja_entrada_id');
            $table->integer('carta_id');
            $table->smallInteger('visto');
            $table->timestamps();
        });

        Schema::create('cartas_bandeja_salidas', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('bandeja_salida_id');
            $table->integer('carta_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bandeja_entradas');
        Schema::dropIfExists('bandeja_salidas');
        Schema::dropIfExists('cartas_bandeja_entradas');
        Schema::dropIfExists('cartas_bandeja_salidas');
    }
}
