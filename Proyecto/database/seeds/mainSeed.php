<?php

use Illuminate\Database\Seeder;

class mainSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //roles
        \DB::table('roles')->insert([
            'id' => 1,
            'rol' => "Editor"
        ]);

        \DB::table('roles')->insert([
            'id' => 2,
            'rol' => "Especialista"
        ]);

        \DB::table('roles')->insert([
            'id' => 3,
            'rol' => "Administrador"
        ]);

        //areas

        \DB::table('areas')->insert([
            'id' => 1,
            'nombre' => "Medicina",
            'diccionario' => "[\"cabeza\",\"cansancio\",\"dolor\",\"gripe\",\"rodilla\"]"
        ]);

        \DB::table('areas')->insert([
            'id' => 2,
            'nombre' => "Psicologia",
            'diccionario' => "[\"ansioso\",\"deprimido\",\"feliz\",\"nervioso\",\"triste\"]"
        ]);

        \DB::table('areas')->insert([
            'id' => 3,
            'nombre' => "Artes",
            'diccionario' => "[\"color\",\"dibujo\",\"flor\",\"imaginacion\",\"pintura\"]"
        ]);

        \DB::table('areas')->insert([
            'id' => 4,
            'nombre' => "Varios",
            'diccionario' => "[]"
        ]);

        //profesiones

        \DB::table('profesions')->insert([
            'id' => 1,
            'nombre' => "OFTALMOLOGO",
            'area_id' => 1
        ]);

        \DB::table('profesions')->insert([
            'id' => 2,
            'nombre' => "MEDICO",
            'area_id' => 1
        ]);

        \DB::table('profesions')->insert([
            'id' => 3,
            'nombre' => "PSICOLOGO",
            'area_id' => 2
        ]);

        \DB::table('profesions')->insert([
            'id' => 4,
            'nombre' => "PSIQUIATRA",
            'area_id' => 2
        ]);

        \DB::table('profesions')->insert([
            'id' => 5,
            'nombre' => "PINTOR",
            'area_id' => 3
        ]);

        \DB::table('profesions')->insert([
            'id' => 6,
            'nombre' => "BAILARIN",
            'area_id' => 3
        ]);

        \DB::table('profesions')->insert([
            'id' => 7,
            'nombre' => "ADMINISTRADOR DEL SISTEMA",
            'area_id' => 4
        ]);

        //usuarios

        \DB::table('usuarios')->insert([
            'id' => 1,
            'nombre_completo' => "Maria Luz Rojas Prieto",
            'correo' => 'mari.RP@hotmail.com',
            'contrasenha' => 'mariluz123',
            'direccion' => '6 de agosto',
            'profesion_id' => 5,
            'telefono' => '78923451',
            'rol_id' => '2',
            'activo' => '1'
        ]);

        \DB::table('bandeja_entradas')->insert([
            'id' => 1,
            'duenho_id' => 1,
            'area_id' => 3
        ]);

        \DB::table('bandeja_salidas')->insert([
            'id' => 1,
            'dueño_id' => 1,
            'tipo_duenho' => 1
        ]);

        \DB::table('usuarios')->insert([
            'id' => 2,
            'nombre_completo' => "Joshua Nostas",
            'correo' => 'joshnosP@gmail.com',
            'contrasenha' => 'violin321',
            'direccion' => 'Beijing',
            'profesion_id' => 2,
            'telefono' => '65374912',
            'rol_id' => '2',
            'activo' => '1'
        ]);

        \DB::table('bandeja_entradas')->insert([
            'id' => 2,
            'duenho_id' => 2,
            'area_id' => 1
        ]);

        \DB::table('bandeja_salidas')->insert([
            'id' => 2,
            'dueño_id' => 2,
            'tipo_duenho' => 1
        ]);

        \DB::table('usuarios')->insert([
            'id' => 3,
            'nombre_completo' => "Fernanda Soria Cruz",
            'correo' => 'fsc@yahoo.com.es',
            'contrasenha' => 'fercita',
            'direccion' => 'Av. America',
            'profesion_id' => 3,
            'telefono' => '76592412',
            'rol_id' => '1',
            'activo' => '1'
        ]);

        \DB::table('bandeja_entradas')->insert([
            'id' => 3,
            'duenho_id' => 3,
            'area_id' => 2
        ]);

        \DB::table('bandeja_salidas')->insert([
            'id' => 3,
            'dueño_id' => 3,
            'tipo_duenho' => 1
        ]);

        \DB::table('usuarios')->insert([
            'id' => 4,
            'nombre_completo' => "Patricia",
            'correo' => 'paty@gmail.es',
            'contrasenha' => 'tis123',
            'direccion' => 'Av. Libertador',
            'profesion_id' => 7,
            'telefono' => '67129034',
            'rol_id' => '3',
            'activo' => '1'
        ]);

        \DB::table('bandeja_entradas')->insert([
            'id' => 4,
            'duenho_id' => 4,
            'area_id' => 4
        ]);

        \DB::table('bandeja_salidas')->insert([
            'id' => 4,
            'dueño_id' => 4,
            'tipo_duenho' => 1
        ]);

        //avatars
        \DB::table('avatars')->insert([
            'id' => 1,
            'nombre_avatar' => "Thor",
            'nombre_imagen' => "../imagenes/1.png"
        ]);

        \DB::table('avatars')->insert([
            'id' => 2,
            'nombre_avatar' => "Iron Man",
            'nombre_imagen' => "../imagenes/2.png"
        ]);

        \DB::table('avatars')->insert([
            'id' => 3,
            'nombre_avatar' => "Hombre Araña",
            'nombre_imagen' => "../imagenes/3.png"
        ]);

        \DB::table('avatars')->insert([
            'id' => 4,
            'nombre_avatar' => "Capitan America",
            'nombre_imagen' => "../imagenes/4.png"
        ]);

        \DB::table('avatars')->insert([
            'id' => 5,
            'nombre_avatar' => "Mujer Maravilla",
            'nombre_imagen' => "../imagenes/5.png"
        ]);

        \DB::table('avatars')->insert([
            'id' => 6,
            'nombre_avatar' => "Viuda Negra",
            'nombre_imagen' => "../imagenes/6.png"
        ]);

        \DB::table('avatars')->insert([
            'id' => 7,
            'nombre_avatar' => "Pantera Negra",
            'nombre_imagen' => "../imagenes/7.png"
        ]);

        \DB::table('avatars')->insert([
            'id' => 8,
            'nombre_avatar' => "Hulk",
            'nombre_imagen' => "../imagenes/8.png"
        ]);

        \DB::table('avatars')->insert([
            'id' => 9,
            'nombre_avatar' => "Rey Leon",
            'nombre_imagen' => "../imagenes/9.png"
        ]);

        \DB::table('avatars')->insert([
            'id' => 10,
            'nombre_avatar' => "Batman",
            'nombre_imagen' => "../imagenes/10.png"
        ]);

    }

}
