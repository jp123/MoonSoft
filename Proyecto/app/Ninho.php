<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ninho extends Model
{
    protected $fillable = ['nombre_usuario', 'activo', 'avatar_id'];
}
