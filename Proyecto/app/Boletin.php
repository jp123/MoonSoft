<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Boletin extends Model
{
    protected $table = 'boletines';
    protected $fillable = ['carta_id', 'editor_id', 'especialista_id', 'archivo_id', 'aprobado', 'publicado', 'titulo'];
}
