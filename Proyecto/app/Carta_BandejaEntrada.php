<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Carta_BandejaEntrada extends Model
{
    protected $table = 'cartas_bandeja_entradas';
    protected $fillable = ['bandeja_entrada_id', 'carta_id', 'visto'];
}
