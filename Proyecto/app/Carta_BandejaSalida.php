<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Carta_BandejaSalida extends Model
{
    protected $table = 'cartas_bandeja_salidas';
    protected $fillable = ['bandeja_salida_id', 'carta_id'];
}
