<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BandejaEntrada extends Model
{
    protected $table = 'bandeja_entradas';
    protected $fillable = ['duenho_id', 'area_id'];
}
