<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BandejaSalida extends Model
{
    protected $table = 'bandeja_salidas';
    protected $fillable = ['dueño_id', 'tipo_duenho'];
}
