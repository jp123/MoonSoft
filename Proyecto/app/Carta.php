<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Carta extends Model
{
    protected $fillable = ['contenido', 'titulo', 'emisor_id', 'tipo_emisor', 'receptor_id', 'area_id', 'fecha_emision', 'archivo_id', 'carta_respondida_id', 'solicitud', 'atendido'];
}
