<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Carta_BandejaEntrada;
use App\BandejaEntrada;

class Carta_BandejaEntradaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function borrarRegistro($idCarta, $idUsuario, $tipo){
        if($tipo == 0){
            $bandeja = BandejaEntrada::where('duenho_id', $idUsuario)->where('area_id', -1)->get()->first();
        }else{
            $bandeja = BandejaEntrada::where('duenho_id', $idUsuario)->where('area_id', '!=', -1)->get()->first();
        }
        $registro = Carta_BandejaEntrada::where('bandeja_entrada_id', $bandeja->id)->where('carta_id', $idCarta)->get()->first();
        $destroy = Carta_BandejaEntrada::destroy($registro->id);
        
    }
}
