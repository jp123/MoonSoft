<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Area;
use App\Profesion;

class AreaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $area = new Area;
        $area->nombre = $request->nombre;
        $area->diccionario = $request->diccionario;
        $area->save();
        $this->guardarProfesiones($area->id, $request->profesiones);
        return $area;
    }

    private function guardarProfesiones($areaId, $profesiones){
        if(count($profesiones)>0){
            foreach($profesiones as $profesion){
                $prof = new Profesion;
                $prof->nombre = $profesion;
                $prof->area_id = $areaId;
                $prof->save();
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $area = Area::find($id);
        $area->parametros = json_decode($area->diccionario);
        unset($area->diccionario);
        $area->profesiones = Profesion::where('area_id', $id)->get();
        return $area;
    }

    public function showAll()
    {
        return Area::all();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $area = Area::find($id);
        $area->nombre = $request->nombre;
        $area->diccionario = $request->diccionario;
        $area->save();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
