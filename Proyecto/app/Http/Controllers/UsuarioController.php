<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Usuario;
use App\Role;
use App\Profesion;
use App\BandejaEntrada;
use App\BandejaSalida;
use App\Area;

class UsuarioController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    public function getDefaultRolId()
    {
        $defaultRol = 'Especialista';
        $rol = Role::where("rol", $defaultRol)->get()->first();
        return $rol->id;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $usuario = $this->crearUsuario($request);
        $usuario->save();
        $this->crearBandejaDeEntrada($usuario);
        $this->crearBandejaDeSalida($usuario);
    }

    private function crearUsuario($datos){
        $defaultRolId = $this->getDefaultRolId();
        $usuario = new usuario;
        $usuario->nombre_completo = $datos->nombre_completo;
        $usuario->contrasenha = $datos->contrasenha;
        $usuario->profesion_id = $datos->profesion_id;
        $usuario->correo = $datos->correo;
        $usuario->telefono = $datos->telefono;
        $usuario->direccion = $datos->direccion;
        $usuario->rol_id = $defaultRolId;
        $usuario->activo = 1;
        return $usuario;
    }

    private function crearBandejaDeEntrada($usuario){
        $area_id = Profesion::find($usuario->profesion_id)->area_id;
        $bandejaEntrada = new BandejaEntrada;
        $bandejaEntrada->duenho_id = $usuario->id;
        $bandejaEntrada->area_id = $area_id;
        $bandejaEntrada->save();
    }

    private function crearBandejaDeSalida($usuario){
        $bandejaSalida = new BandejaSalida;
        $bandejaSalida->dueño_id = $usuario->id;
        $bandejaSalida->tipo_duenho = 1;
        $bandejaSalida->save();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $usuario = Usuario::find($id);
        $profesion_id = $usuario->profesion_id;
        $nombre_profesion = Profesion::find($profesion_id)->nombre;
        $usuario->profesion = $nombre_profesion;
        $usuario->area = Area::find(Profesion::find($profesion_id)->area_id)->nombre;
        return $usuario;
    }

    public function showAll()
    {
        $usuarios = Usuario::all();
        foreach($usuarios as $usuario){
            $rol = Role::find($usuario->rol_id);
            $usuario->rol = $rol->rol;
        }
        return $usuarios;
    }

    public function verify(Request $request)
    {
        $correo = $request->correo;
        $contraMandada = $request->contrasenha;
        $usuario = Usuario::where("correo", $correo)->get()->first();;
        if($usuario != NULL){
            return response()->json([
                'correcto' => $contraMandada == $usuario->contrasenha,
                'rol' => $usuario->rol_id,
                'id_usuario' => $usuario->id
            ]);
        }else{
            return response()->json([], 400);
        }
    }

    public function changeRolUser(Request $request)
    {
        $user_id = $request->user_id;
        $new_rol_id = $request->rol_id;
        $usuario = Usuario::find($user_id);
        $usuario->rol_id = $new_rol_id;
        $usuario->save();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $usuario = Usuario::find($id);
        
        $usuario->nombre_completo = $request->nombre_completo;
        $usuario->contrasenha = $request->contrasenha;
        $usuario->correo = $request->correo;
        $usuario->telefono = $request->telefono;
        $usuario->direccion = $request->direccion;
        $usuario->rol_id = $request->rol_id;
        $usuario->activo = $request->activo;
        $usuario->save();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
