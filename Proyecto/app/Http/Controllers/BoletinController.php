<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Boletin;
use App\Comentario;
use App\Usuario;
use App\Archivo;
use App\Area;
use App\Carta;
use App\Profesion;

class BoletinController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $boletin = new Boletin();
        $boletin->carta_id = $request->carta_id;
        $boletin->editor_id = $request->editor_id;
        $boletin->especialista_id = $request->especialista_id;
        $boletin->archivo_id = $request->archivo_id;
        $boletin->titulo = $request->titulo;
        $boletin->aprobado = 0;
        $boletin->publicado = 0;
        $boletin->alerta = 1;
        $boletin->save();
        return $boletin->id;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $boletin = Boletin::find($id);
        $this->recuperarArchivo($boletin);
        $boletin->comentarios = $this->cargarComentarios($id);
        return $boletin;
    }

    public function publics(){
        $boletines = Boletin::where('publicado', 1)->get();
        foreach($boletines as $boletin){
            $this->recuperarArea($boletin);
            $this->recuperarArchivo($boletin);
        }
        return $boletines;
    }

    public function recuperarArea($boletin){
        $carta = Carta::find($boletin->carta_id);
        $usuario = Usuario::find($carta->emisor_id);
        $profesion = Profesion::find($usuario->profesion_id);
        $boletin->area = Area::find($profesion->area_id)->nombre;
    }

    private function recuperarArchivo($boletin){
        $archivoId = $boletin->archivo_id;
        if($archivoId > 0){
            $archivo = Archivo::find($archivoId);
            $boletin->archivo = $archivo;
        }
    }

    private function cargarComentarios($boletinId){
        $comentarios = Comentario::where('boletin_id', $boletinId)->get();
        foreach($comentarios as $comentario){
            $usuario = Usuario::find($comentario->autor);
            $comentario->autor = $usuario;
        }
        return $comentarios;
    }

    public function boletinesDe($id, $tipo){
        $boletines = [];
        if($tipo == 0){
            $boletines = Boletin::where('editor_id', $id)->get();
        }
        else{
            $boletines = Boletin::where('especialista_id', $id)->get();
        }
        foreach($boletines as $boletin){
            $this->cargarInvolucrados($boletin);
        }
        return $boletines;
    }

    public function cargarInvolucrados($boletin){
        $editor = Usuario::find($boletin->editor_id);
        $especialista = Usuario::find($boletin->especialista_id);
        $boletin->editor = $editor->nombre_completo;
        $boletin->revisor = $especialista->nombre_completo;
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $boletin = Boletin::find($id);
        $boletin->titulo = $request->titulo;
        $boletin->aprobado = $request->aprobado;
        $boletin->publicado = $request->publicado;
        $boletin->alerta = $request->alerta;
        $boletin->save();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
