<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Ninho;
use App\Avatar;
use App\BandejaSalida;
use App\BandejaEntrada;

class NinhoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $ninho = new Ninho;
        $ninho->nombre_usuario = $request->nombre_usuario;
        $ninho->avatar_id = $request->avatar_id;
        $ninho->activo = 1;
        $ninho->save();
        $this->crearBandejaDeEntrada($ninho);
        $this->crearBandejaDeSalida($ninho);
    }

    private function crearBandejaDeEntrada($usuario){
        $area_id = -1;
        $bandejaEntrada = new BandejaEntrada;
        $bandejaEntrada->duenho_id = $usuario->id;
        $bandejaEntrada->area_id = $area_id;
        $bandejaEntrada->save();
    }

    private function crearBandejaDeSalida($usuario){
        $bandejaSalida = new BandejaSalida;
        $bandejaSalida->dueño_id = $usuario->id;
        $bandejaSalida->tipo_duenho = 0;
        $bandejaSalida->save();
    }

    public function getLastId(){
        $max = Ninho::max('id');
        if($max == NULL){
            return 0;
        }
        return $max;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $ninho = Ninho::where('nombre_usuario', $id)->get()->first();
        if($ninho != null){
            $ninho->avatar = $this->chargeAvatar($ninho->avatar_id);
            return $ninho;
        }else{
            return response()->json([], 400);
        }
    }

    private function chargeAvatar($id){
        return Avatar::find($id);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
