<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Carta;
use App\Usuario;
use App\Ninho;
use App\Carta_BandejaEntrada;
use App\Carta_BandejaSalida;
use App\BandejaEntrada;
use App\BandejaSalida;
use App\Area;
use App\Archivo;

class CartaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    public function guardarCartaNinho(Request $request){
        date_default_timezone_set('America/Caracas');
        $fecha = date('Y/m/d h:i:s', time());

        $carta = new Carta;
        $carta->contenido = $request->contenido;
        $carta->titulo = " ";
        $carta->emisor_id = $request->emisor_id;
        $carta->tipo_emisor = 0;
        $carta->receptor_id = $request->receptor_id;
        $carta->tipo_receptor = $request->tipo_receptor;
        $carta->area_id = -1;
        $carta->fecha_emision = $fecha;
        $carta->archivo_id = $request->archivo_id;
        $carta->carta_respondida_id = 0;
        $carta->solicitud = 0;
        $carta->atendido = 0;
        $carta->save();
        if($request->receptor_id == 0){
            $this->clasificar($carta);
        }else{
            $this->actualizarBandejas($carta);
        }
    }

    public function guardarCarta(Request $request){
        date_default_timezone_set('America/Caracas');
        $fecha = date('Y/m/d h:i:s', time());

        $carta = new Carta;
        $carta->contenido = $request->contenido;
        $carta->titulo = $request->titulo;
        $carta->emisor_id = $request->emisor_id;
        $carta->tipo_emisor = 1;
        $carta->receptor_id = $request->receptor_id;;
        $carta->tipo_receptor = $request->tipo_receptor;
        $carta->area_id = 0;
        $carta->fecha_emision = $fecha;
        $carta->archivo_id = $request->archivo_id;
        $carta->carta_respondida_id = $request->carta_respondida_id;
        $carta->solicitud = $request->solicitud;
        $carta->atendido = 0;
        $carta->save();
        $this->actualizarBandejas($carta);
    }

    private function clasificar($carta){
        $carta->area_id = $this->masCoincidencias($carta->contenido);
        $carta->save();
        $this->actualizarBandejas($carta);
    }

    private function masCoincidencias($texto){
        $tokens = explode(" ", $texto);
        $areas = Area::all();
        $maxAreaId = 0;
        $maxCoincidencias = 0;
        foreach($areas as $area){
            $array = json_decode($area->diccionario);
            $coincidencia = $this->coincidencias($tokens, $array);
            if($maxCoincidencias < $coincidencia){
                $maxAreaId = $area->id;
                $maxCoincidencias = $coincidencia;
            }
        }
        if($maxAreaId == 0){
            $varios = Area::where('nombre', 'Varios')->get()->first();
            $maxAreaId = $varios->id;
        }
        return $maxAreaId;
    }

    private function encontrar($searched, $array){
        return in_array($searched, $array);
    }
      
    private function coincidencias($tokens, $array){
        $cont = 0;
        foreach($tokens as $token){
            if($this->encontrar($token, $array)){
                $cont++;
            }
        }
        return $cont;
    }

    private function actualizarBandejas($carta){
        $this->actualizarBandejaEntradaReceptor($carta);
        $this->actualizarBandejaSalidaEmisor($carta);
    }

    private function actualizarBandejaEntradaReceptor($carta){
        if($carta->receptor_id == 0){
            $this->actualizarBandejaEntradaPorArea($carta);
        }else{
            $this->actualizarBandejaEntradaDirecta($carta);
        }
    }

    private function actualizarBandejaEntradaPorArea($carta){
        $area_id = $carta->area_id;
        $bandejas = BandejaEntrada::where('area_id', $area_id)->get();
        foreach ($bandejas as $bandejaEntrada) {
            $mediador = new Carta_BandejaEntrada;
            $mediador->bandeja_entrada_id = $bandejaEntrada->id;
            $mediador->carta_id = $carta->id;
            $mediador->visto = 0;
            $mediador->save();
        }
    }

    private function actualizarBandejaEntradaDirecta($carta){
        $receptor = $carta->receptor_id;
        $tipo_receptor = $carta->tipo_receptor;
        if($tipo_receptor == -1){
            $bandejaEntrada = BandejaEntrada::where('duenho_id', $receptor)->where('area_id', $tipo_receptor)->get()->first();
        }else{
            $bandejaEntrada = BandejaEntrada::where('duenho_id', $receptor)->where('area_id', '!=', -1)->get()->first();
        }
        $mediador = new Carta_BandejaEntrada;
        $mediador->bandeja_entrada_id = $bandejaEntrada->id;
        $mediador->carta_id = $carta->id;
        $mediador->visto = 0;
        $mediador->save();
    }

    private function actualizarBandejaSalidaEmisor($carta){
        $tipo_emisor = $carta->tipo_emisor;
        $bandejaSalida = BandejaSalida::where('dueño_id', $carta->emisor_id)->where('tipo_duenho', $tipo_emisor)->get()->first();
        $mediador = new Carta_BandejaSalida;
        $mediador->bandeja_salida_id = $bandejaSalida->id;
        $mediador->carta_id = $carta->id;
        $mediador->save();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $conversacion = array();
        $carta =  $this->recuperarCarta($id);
        $conversacion = $this->cargarConversacion($carta->carta_respondida_id, $conversacion);
        return response()->json([
            'carta' => $carta,
            'conversacion' => $conversacion
        ]);
    }

    private function cargarConversacion($id, $conversacion){
        while($id > 0){
            $carta = $this->recuperarCarta($id);
            array_push($conversacion, $carta);
            $id = $carta->carta_respondida_id;
        }
        return $conversacion;
    }

    private function recuperarCarta($id){
        $carta = Carta::find($id);
        $carta->emisor = $this->recuperarUsuario($carta->emisor_id, $carta->tipo_emisor);
        if($carta->tipo_emisor == 0){
            $carta->receptor = $this->recuperarUsuario($carta->area_id, 2);
        }else{
            $carta->receptor = $this->recuperarUsuario($carta->receptor_id, 1);
        }
        if($carta->tipo_receptor == -1){
            $carta->receptor = $this->recuperarUsuario($carta->receptor_id, 0);
        }
        $this->recuperarArchivo($carta);
        return $carta;
    }

    private function recuperarArchivo($carta){
        $archivoId = $carta->archivo_id;
        if($archivoId > 0){
            $archivo = Archivo::find($archivoId);
            $carta->archivo = $archivo;
        }
    }

    private function recuperarUsuario($emisor, $tipoEmisor){
        $response = new \stdClass();
        if($tipoEmisor == 1){
            $usuario = Usuario::find($emisor);
            $response->nombre = $usuario->nombre_completo;
        }else if($tipoEmisor == 0){
            $usuario = Ninho::find($emisor);
            $response->nombre = $usuario->nombre_usuario;
        }else{
            $area = Area::find($emisor);
            $response->nombre = "Area ".$area->nombre;
        }
        return $response;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $carta = Carta::find($id);
        $carta->area_id = $request->area_id;
        $carta->save();
        $this->actualizarBandejaEntradaReceptor($carta);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
