<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\BandejaSalida;
use App\Carta_BandejaSalida;
use App\Carta;
use App\Usuario;
use App\Ninho;

class BandejaSalidaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        
    }

    public function mostrarPorTipo($usuarioId, $tipo){
        $bandeja = BandejaSalida::where('dueño_id', $usuarioId)->where('tipo_duenho', $tipo)->get()->first();
        return $this->cartasDeLaBandeja($bandeja->id);
    }

    private function cartasDeLaBandeja($bandejaId){
        $mediadores = Carta_BandejaSalida::where('bandeja_salida_id', $bandejaId)->get();
        $cartas = array();

        foreach ($mediadores as $mediador) {
            $carta = Carta::find($mediador->carta_id);
            if($carta->receptor_id > 0 && $carta->tipo_receptor > -1){
                $carta->receptor = $this->recuperarInfoUsuario($carta->receptor_id);
            }else if($carta->receptor_id > 0){
                $carta->receptor = $this->recuperarInfoNinho($carta->receptor_id);
            }
            array_push($cartas, $carta);
        }
        return $cartas;
    }

    private function recuperarInfoUsuario($receptor){
        $response = new \stdClass();
        $usuario = Usuario::find($receptor);
        $response->nombre = $usuario->nombre_completo;
        return $response;
    }

    private function recuperarInfoNinho($receptor){
        $response = new \stdClass();
        $usuario = Ninho::find($receptor);
        $response->nombre = $usuario->nombre_usuario;
        return $response;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
