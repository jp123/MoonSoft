<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\BandejaEntrada;
use App\Carta_BandejaEntrada;
use App\Usuario;
use App\Ninho;
use App\Carta;

class BandejaEntradaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    public function marcarVisto($idUsuario, $idCarta){
        $bandeja = BandejaEntrada::where('duenho_id', $idUsuario)->get()->first();
        $mediador = Carta_BandejaEntrada::where('bandeja_entrada_id', $bandeja->id)->where('carta_id', $idCarta)->get()->first();
        $mediador->visto = 1;
        $mediador->save();
    }

    public function show($id, $tipo)
    {
        if($tipo == 0){
            $bandeja = BandejaEntrada::where('duenho_id', $id)->where('area_id', -1)->get()->first();
        }else{
            $bandeja = BandejaEntrada::where('duenho_id', $id)->where('area_id', '!=', -1)->get()->first();
        }
        return $this->cartasDeLaBandeja($bandeja->id);
    }

    private function cartasDeLaBandeja($bandejaId){
        $mediadores = Carta_BandejaEntrada::where('bandeja_entrada_id', $bandejaId)->get();
        $cartas = array();

        foreach ($mediadores as $mediador) {
            $carta = Carta::find($mediador->carta_id);
            $carta->visto = $mediador->visto;
            $carta->emisor = $this->recuperarInfoUsuario($carta->emisor_id, $carta->tipo_emisor);
            array_push($cartas, $carta);
        }
        return $cartas;
    }

    private function recuperarInfoUsuario($emisor, $tipoEmisor){
        $response = new \stdClass();
        if($tipoEmisor == 1){
            $usuario = Usuario::find($emisor);
            $response->nombre = $usuario->nombre_completo;
        }else{
            $usuario = Ninho::find($emisor);
            $response->nombre = $usuario->nombre_usuario;
        }
        return $response;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
