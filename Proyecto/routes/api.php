<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
Route::resource('Usuario', 'UsuarioController');
Route::resource('Area', 'AreaController');
Route::resource('Profesion', 'ProfesionController');
Route::resource('Ninho', 'NinhoController');
Route::resource('Carta', 'CartaController');
Route::resource('BandejaEntrada', 'BandejaEntradaController');
Route::resource('BandejaSalida', 'BandejaSalidaController');
Route::resource('Carta_BandejaEntrada', 'Carta_BandejaEntradaController');
Route::resource('Carta_BandejaSalida', 'Carta_BandejaSalidaController');
Route::resource('Avatar', 'AvatarController');
Route::resource('Archivo', 'ArchivoController');
Route::resource('Boletin', 'BoletinController');
Route::resource('Comentario', 'ComentarioControlador');

Route::post('/login','UsuarioController@verify');
Route::post('/visto/{idUsuario}/{idCarta}','BandejaEntradaController@marcarVisto');
Route::get('/todosUsuarios','UsuarioController@showAll');
Route::get('/roles','RolController@showAll');
Route::put('/cambiarRol','UsuarioController@changeRolUser');
Route::get('/Areas','AreaController@showAll');
Route::get('/profesiones','ProfesionController@showAll');
Route::get('/BandejaSalida/{usuarioId}/{tipo}','BandejaSalidaController@mostrarPorTipo');
Route::post('/enviarNinho','CartaController@guardarCartaNinho');
Route::post('/enviar','CartaController@guardarCarta');
Route::delete('/borrarEntrada/{idCarta}/{idUsuario}/{tipo}','Carta_BandejaEntradaController@borrarRegistro');
Route::delete('/borrarSalida/{idCarta}/{idUsuario}/{tipo}','Carta_BandejaSalidaController@borrarRegistro');
Route::get('/Avatars','AvatarController@showAll');
Route::get('/maxId','NinhoController@getLastId');
Route::get('/Boletines/{id}/{tipo}','BoletinController@boletinesDe');
Route::get('/BoletinesPublicos','BoletinController@publics');
Route::get('/BandejaEntrada/{id}/{tipo}','BandejaEntradaController@show');