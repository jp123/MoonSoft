Dado(/^que estoy en la pagina principal$/) do
    page.driver.browser.manage.window.maximize
    visit ('http://localhost:8000/#!/')
  end

Cuando("presiono sobre el enlace {string}") do |link_name|
    sleep(2)
    click_link(link_name)
end

Cuando("lleno el campo de correo electronico con {string}") do |correo|
    fill_in('email', :with => correo)
    sleep(2)
end
  
Cuando("lleno el campo de contraseña con {string}") do |contrasenha|
    fill_in('pwd', :with => contrasenha)
    sleep(2)
end
  
Cuando("presiono el boton {string}") do |boton|
    sleep(2)
    click_button(boton)
end

Cuando("lleno el campo {string} con {string}") do |selector, contenido|
    fill_in(selector, :with => contenido)
end
  
Entonces("veo el titulo {string}") do |titulo|
    actualError = find(:xpath, '/html/body/div/div[2]/h1')
    if actualError.text != titulo
    	raise "Titulo deberia: " + titulo	
    end
end

Cuando("en el campo {string} selecciono la opcion {string}") do |string, string2|
     # Write code here that turns the phrase above into concrete actions
end

Cuando("lleno el campo {string} seleccionando la opcion {string}") do |selector, opcion|
    select opcion, :from => selector
    sleep(2)
end

Cuando("veo el correo de {string}") do |emisor|
    actualError = find(:xpath, '/html/body/div/div[2]/div/table/tbody/tr[1]/td[2]')
    if actualError.text != emisor
    	raise "El correo deberia ser de : " + emisor	
    end    
end
Cuando("hago click al correo mas reciente") do
    correo = find(:xpath, '/html/body/div/div[2]/div/table/tbody/tr[1]').click
end

Entonces("veo la informacion del correo") do

end
  