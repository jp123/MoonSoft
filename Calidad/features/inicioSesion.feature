# language: es
Característica: Ingresar al sistema
   Como un usuario
   Quiero ingresar al sistema
   Para poder revisar las cartas recibidas

   Escenario: Registrarse en el sistema
    Dado que estoy en la pagina principal
    Cuando presiono el boton "Iniciar Sesion"
    Y presiono sobre el enlace "Registrate"
    Y lleno el campo "nombre" con "Lindsay Montaño"
    Y lleno el campo "direccion" con "2da Circunvalacion"
    Y lleno el campo "telefono" con "78787878"
    Y en el campo "profesion" selecciono la opcion "Oftalmologo"
    Y lleno el campo "email" con "Lindsay@gmail.com"
    Y lleno el campo "contraseña" con "lindsay"
    Y lleno el campo "repetir contraseña" con "lindsay"
    Y presiono el boton "Aceptar"
    Entonces veo el titulo "Bienvenido"

   Escenario: Ingresar al sistema
    Dado que estoy en la pagina principal
    Cuando presiono el boton "Iniciar Sesion"
    Y lleno el campo de correo electronico con "Lindsay@gmail.com"
    Y lleno el campo de contraseña con "lindsay"
    Y presiono el boton "Aceptar"
    Entonces veo el titulo "Bienvenido"


