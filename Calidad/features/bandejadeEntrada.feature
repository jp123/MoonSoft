# language: es
Característica: Recibir carta
   Como un usuario
   Quiero Recibir una carta
   Para poder ver nuevos mensajes 

    Antecedentes:
    Dado que estoy en la pagina principal
    Cuando presiono el boton "Iniciar Sesion"
    Y lleno el campo de correo electronico con "paty@gmail.es"
    Y lleno el campo de contraseña con "tis123"
    Y presiono el boton "Aceptar"

    Escenario: Recibir una carta
    Y presiono sobre el enlace "Bandeja de Entrada"
    Y veo el correo de "Lindsay Montaño"
    Y hago click al correo mas reciente
    Entonces veo la informacion del correo
    
     