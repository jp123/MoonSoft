# language: es
Característica: Enviar carta
   Como un usuario
   Quiero enviar una carta
   Para poder responder al editor

    Antecedentes:
    Dado que estoy en la pagina principal
    Cuando presiono el boton "Iniciar Sesion"
    Y lleno el campo de correo electronico con "Lindsay@gmail.com"
    Y lleno el campo de contraseña con "lindsay"
    Y presiono el boton "Aceptar"

    Escenario: Redactar una carta
    Y presiono sobre el enlace "Bandeja de Salida"
    Y presiono el boton "Redactar"
    Y lleno el campo "lista de profesionales" seleccionando la opcion "paty@gmail.es"
    Y lleno el campo "asunto" con "Prueba"
    Y lleno el campo "area de Mensaje" con "Probando 1 2 3"
    Y presiono el boton "enviar carta"
     